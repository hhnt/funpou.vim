autocmd BufRead,BufNewFile *.funpou imap <CR> <CR><C-R>=InsertJobLogging()<CR>
autocmd BufRead,BufNewFile *.funpou set filetype=funpou

function! InsertJobLogging()
    let DATETIME = strftime("%y-%m-%d_%H:%M:%S ")
    let JOB_FULLNAME  = expand("%:p")
    let regex = '[^\/]*\ze\.funpou'
    let JOBNAME = matchstr(JOB_FULLNAME,regex)
    return DATETIME.'['.JOBNAME.'] '
endfunction
