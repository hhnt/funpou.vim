" regex match
syntax match fpDate /^\d\d-\d\d-\d\d_\d\d:\d\d:\d\d/
syntax match fpJobName /\[.*\]/
syntax match fpTag /@\w*/

" highlight groups
highlight link fpDate Statement
highlight link fpJobName String
highlight link fpTag Constant 

let b:current_syntax = "funpou"
